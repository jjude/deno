// Run this locally with: deno run --allow-net index.ts
import PocketBase from 'https://esm.sh/pocketbase'
import { load } from "https://deno.land/std@0.214.0/dotenv/mod.ts";
const env = await load();
const pburl = env["PB_URL"];
const pbadmin = env['PB_ADMIN']
const pbadminpwd = env['PB_ADMIN_PASSWORD']

const pb = new PocketBase(pburl)
// we will use admin auth since there is only one user as of now
const authData = await pb.admins.authWithPassword(pbadmin, pbadminpwd);
console.log("authdata: ", authData.token)
// after the above you can also access the auth data from the authStore
console.log(pb.authStore.isValid);
console.log(pb.authStore.token);
console.log(pb.authStore.model.id);

// "logout" the last authenticated account
pb.authStore.clear();