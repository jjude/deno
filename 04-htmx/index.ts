// Run this locally with: deno run --allow-net --allow-read index.ts
import { Hono } from "https://deno.land/x/hono/mod.ts";
import vento from "https://deno.land/x/vento@v0.9.1/mod.ts";

const app = new Hono();
const env = vento();
const val = { name: "Joseph", phone: "123456", city: "Chandigarh" };
const indextmpl = await env.load("index.vto");
const ndx = await indextmpl(val);
const cardtmpl = await env.load("card.vto")
const cardres = await cardtmpl(val)

app.get("/", (c) => c.html(ndx.content));
app.get("/getinfo", (c) => c.html(cardres.content));

Deno.serve(app.fetch);
