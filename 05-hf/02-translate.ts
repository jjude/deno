import { load } from "https://deno.land/std@0.214.0/dotenv/mod.ts";
import { HfInference } from "https://esm.sh/@huggingface/inference"
const env = await load();
const hftoken = env["HF_TOKEN"];

const hf = new HfInference(hftoken)

// Hugging Face Inference API docs: https://huggingface.co/docs/huggingface.js/inference/README

// Text translation
const textToTranslate = "It's an exciting time to be an AI engineer"

// https://huggingface.co/facebook/mbart-large-50-many-to-many-mmt#languages-covered

const textTranslationResponse = await hf.translation({
  model: 'facebook/mbart-large-50-many-to-many-mmt',
  inputs: textToTranslate,
  parameters: {
      src_lang: "en_XX",
      tgt_lang: "ta_IN" // tamil translation is no good
  }
})

const translation = textTranslationResponse.translation_text
console.log("\ntranslation:\n")
console.log(translation)