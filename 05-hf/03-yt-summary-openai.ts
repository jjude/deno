import { YoutubeTranscript } from 'npm:youtube-transcript';
import OpenAI from "https://deno.land/x/openai@v4.20.1/mod.ts";

const yttr = await YoutubeTranscript.fetchTranscript('txfhZfSuX94')
// await Deno.writeTextFile("yttranscript.txt", JSON.stringify(yttr));
// const text = await Deno.readTextFile("yttranscript.txt");
// const yt = JSON.parse(text);

const openai = new OpenAI();
const prompt = `You will be provided with video captions. Summarize the video in one paragraph. And then list 5 major points discussed in the video and for each point, list 3 important key ideas.`

const keys = Object.keys(yttr);
let full_text = "";
keys.forEach((objKey) => {
  const node = yttr[objKey];
  full_text = full_text + " " + node.text;
});

// await Deno.writeTextFile("ytcaptions.txt",full_text)

const response = await openai.chat.completions.create({
    model: "gpt-3.5-turbo-1106",
    messages: [
      {
        "role": "system",
        "content": prompt
      },
      {
        "role": "user",
        "content": full_text
      }
    ],
    n: 1,
  });

console.log(response.choices[0].message.content)


