import { load } from "https://deno.land/std@0.214.0/dotenv/mod.ts";
import { HfInference } from "https://esm.sh/@huggingface/inference"
const env = await load();
const hftoken = env["HF_TOKEN"];

const hf = new HfInference(hftoken)

const result = await hf.imageToText({
    data: await (await fetch('https://images.unsplash.com/photo-1706550632906-f374a768ef56')).blob(),
    model: 'nlpconnect/vit-gpt2-image-captioning',  
  });

  console.log(result)