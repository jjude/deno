# Deno + Huggingface
## References
- https://huggingface.co/docs/huggingface.js/inference/README
- https://scrimba.com/scrim/cod8248f5adfd6e129582c523
- https://huggingface.co/tasks
- https://autocode.com/openai/api/playground/0.1.1/chat-completions-create/ - helps you find params etc
- refs: https://github.com/franekmagiera/just-tell-me/blob/main/app/src/fetch-youtube-data.ts - what I'm replicating 
- ex: https://youtubetranscript.com/?v=DDbgaO3ZUCo - sample video; KK & I
- https://betterprogramming.pub/rewrite-your-work-in-another-authors-style-using-openai-and-python-45d89b3829d2


## Models to use
- for chat completion: mistralai/Mixtral-8x7B-Instruct-v0.1


## My blogs
- write something like this: https://www.cloudgeometry.io/blog/hugging-face
