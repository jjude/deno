import { YoutubeTranscript } from 'npm:youtube-transcript';
import { load } from "https://deno.land/std@0.214.0/dotenv/mod.ts";
import { HfInference } from "https://esm.sh/@huggingface/inference"
const env = await load();
const hftoken = env["HF_TOKEN"];
const hf = new HfInference(hftoken)

// const yttr = await YoutubeTranscript.fetchTranscript('DDbgaO3ZUCo')

const text = await Deno.readTextFile("yttranscript.txt");
const yttr = JSON.parse(text);
const prompt = `You will be provided with video captions. Summarize the video in one paragraph. `

const keys = Object.keys(yttr);
let full_text = "";
keys.forEach((objKey) => {
  const node = yttr[objKey];
  full_text = full_text + " " + node.text;
});

const res = await hf.textGeneration({
  model: 'gpt2',
  inputs: 'The answer to the universe is'
})

console.log(res.generated_text)