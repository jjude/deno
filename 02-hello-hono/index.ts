// Run this locally with: deno run --allow-net index.ts
import { Hono } from 'https://deno.land/x/hono/mod.ts'

const app = new Hono()

app.get('/', (c) => c.json({text:'Hello world from deno & hono!'}))

Deno.serve(app.fetch)