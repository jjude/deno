# Basic hono project deployed to Deno cloud

Install `deployctl` as per `https://docs.deno.com/deploy/manual`

`deno install -A --no-check -r -f https://deno.land/x/deploy/deployctl.ts`

Create your access token; add it to your `.zshrc` or `.zshprofile` file

`deployctl deploy --project=dneat-calm-hono-01 --prod index.ts`

Now you can see this project on: https://dneat-calm-hono-01.deno.dev