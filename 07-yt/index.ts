import {YouTube} from "https://deno.land/x/youtube_sr/mod.ts";
const url = "https://www.youtube.com/watch?v=M1SAEuoFv7U";

YouTube.getVideo(url)
    .then((vid)=>console.log(vid)) // Video info
    .catch(console.error);