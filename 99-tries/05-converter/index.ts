// Run this locally with: deno run --allow-net --allow-read index.ts
import { Hono } from 'https://deno.land/x/hono/mod.ts'
import vento from "https://deno.land/x/vento@v0.9.1/mod.ts";

const app = new Hono()
const env = vento();
const template = await env.load("index.vto");
const result = await template({ title: "Hello, world!", tmplengine: "vento" });

app.get('/', (c) => c.html(result.content))

Deno.serve(app.fetch)