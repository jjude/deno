# Transcribe a podcast file using AWS services

Reference: https://www.aboutamazon.in/news/tech-blog/these-amazon-ai-tools-can-summarise-your-favourite-podcasts-even-before-you-listen-in

## Steps
- create aws keys
- download .mp3 from podcast
- upload .mp3 to s3 bucket
- setup transcribe job with one of aws model
- get the text back
- summarise the text

For using node lib in deno

```
import { S3 } from "npm:@aws-sdk/client-s3";

await new S3({}).listBuckets({});
```