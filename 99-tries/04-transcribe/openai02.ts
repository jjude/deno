import OpenAI from "https://deno.land/x/openai@v4.20.1/mod.ts";

const openai = new OpenAI();

const txt = `You're looking at goals you are looking at three things the first and foremost is your beliefs who you are how you behave lot of times we think if only I will learn a technique if only I will learn some method I'll be lot more productive and I will achieve my goals but that is not true who you are how you think what do you believe matters a lot so that's the first thing you are your own behaviour and ability second is do you have an environment which enables you to reach your goals you could be as ambitious as you want but if your environment inhibits doing your productive work you are going to have a trouble with that thirdly lastly it is about tools and processes do you have tools and processes that really helps you to achieve again you can work as hard as you want but tools and tools give you leverage tools scan that is why arch said give me a liver long enough I will lift yet so you should be able to do that`
const prompt = `Create vivid images for the reader using sensory language, metaphors, similes, and other figures of speech. Use concrete, specific, and common words in Anglo-Saxon dictionary avoiding complicated sentence structure. Write in first or second person style. Avoid using 3rd person style.`


const response = await openai.chat.completions.create({
  model: "gpt-3.5-turbo",
  messages: [
    {
      "role": "system",
      "content": prompt
    },
    {
      "role": "user",
      "content": txt
    }
  ],
  temperature: 0.7,
  max_tokens: 64,
  top_p: 1,
});

// console.log(response.choices[0].message.content)
console.log(response)