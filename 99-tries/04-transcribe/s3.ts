import { ListBucketsCommand } from "https://cdn.skypack.dev/@aws-sdk/client-s3";
import { config } from "https://deno.land/x/dotenv/mod.ts";
import { ListObjectsCommand, S3Client } from 'npm:@aws-sdk/client-s3@3.264.0';

const client = new S3Client({
    region: 'us-east-1',
    credentials: {
        accessKeyId: "xx",
        secretAccessKey: "xx",
    }
})

async function list() {
    const bkts = await client.send(new ListBucketsCommand({}))
    // const objs = await client.send(new ListObjectsCommand({
    //     Bucket: 'olai'
    // }))

    console.log(bkts)
}

list()