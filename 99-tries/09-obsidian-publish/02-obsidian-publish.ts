/*
program to publish list of notes in markdown (+few additions) as a static site
supports commonmark + wikilinks

1. read config file
2. walk through directory and read only markdown files
3. convert markdown files into html files using a template
*/
// Import the necessary modules
import { exists } from "https://deno.land/std@0.212.0/fs/mod.ts";
import * as path from "https://deno.land/std@0.212.0/path/mod.ts";
import { parse } from "https://deno.land/std@0.212.0/toml/mod.ts";
import vento from "https://deno.land/x/vento@v0.9.1/mod.ts";
import { slug } from "https://deno.land/x/slug/mod.ts";
import {micromark} from 'https://esm.sh/micromark@3'
import { wikiLink } from 'https://deno.land/x/micromark-extension-wikirefs/wiki-link.ts';

interface Config {
  siteName:string;
  siteDescription:string;
  siteAuthor:string
  siteURL:string;
}

function slugifyDir(dir:string):string {
  let ds = ""
  const dirs = dir.split("/")
  dirs.forEach( (val, index)=>{
    if (val == "..") {
      ds = ds + val
    } else {
      ds = ds + slug(val)
    }

    if (index != dirs.length - 1) {
      ds = ds + "/"
    }
  })

  return ds
}

// Read config file
const cf = await Deno.readTextFile("config.toml")
const cdata = parse(cf)

const config: Config = {
  siteName: cdata.site.name,
  siteDescription:cdata.site.description,
  siteAuthor:cdata.user.name,
  siteURL:cdata.site.url,
}

// Read the contents from source folder
const sourceFolder = "./content";
const targetFolder = "./public";
const dirEntries = Deno.readDir(sourceFolder);
const env = vento();
const template = await env.load("entry.vto");

if (await exists(targetFolder)) {
  await Deno.remove(targetFolder, { recursive: true });
}


// Iterate through the markdown files in the folder
// no subfolders are handled
for await (const entry of dirEntries) {
  if (entry.isFile && path.extname(entry.name) == ".md") {
    // If it's a file, read its content
    const filePath = path.join(sourceFolder, entry.name);
    let fc = await Deno.readTextFile(filePath);
    const ast = parse(fc, { extensions: [wikiLink()] });
    const convertedMd = micromark(fc)
    const htmlToSave = await template({ Author:config.siteAuthor, SiteName:config.siteName, Domain:config.siteURL, Content: convertedMd });

    //construct the output folder & filename
    const fname = path.parse(entry.name).name;
    const destDir =slugifyDir(path.join(targetFolder,fname))
    const tfname = path.join(destDir, "index.html")
    //create output index folder & write the file
    await Deno.mkdir(destDir,{ recursive: true })
    await Deno.writeTextFile(tfname,htmlToSave.content)

  }
}
